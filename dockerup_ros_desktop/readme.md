 # Dockerized ROS Dev Environment for NVIDIA Jetson Orin




## Setup Instructions

<br />
### Setup BLUF
:star: Setup Jetson Backpack with NVIDIA Docker. Clone this repo 'dockerup_jetson' into a ROS code repo at the same level as your ROS workspace. Run `setup.sh` from the dockerup dir. Then use `<name_of_parent_dir>` global alias to run the container.
<br />


#### TODO UPDATE Configure directories that the container needs
You will need to map a data directory to the container.
The container will try to map  `$HOME` as the default data directory.
However you can specify a different container at runtime with the `-d` flag: 
Example `./run.sh -d $HOME/MyData/TestData` or by alias `ssudio -d $HOME/MyData/TestData`
NOTE: You must specify the absolute path with the -d flag.


:rotating_light: **IN ORDER TO EXECUTE DOCKER COMMANDS WITHOUT USING SUDO, YOU MUST ADD YOUR USERNAME TO THE DOCKER GROUP!**
Follow the instructions here for creating the `docker` group and adding your username to it. https://docs.docker.com/engine/install/linux-postinstall/

#### Setup the docker container
Run the setup script. 
:no_good: Do NOT use the sudo command to run the script.:no_good: <br/>
`<name_of_parent_dir>/dockerup/setup.sh`

Hopefully everything sets up correctly [fingers crossed!!!] 

### Usage
#### Run the container
`<name_of_parent_dir>/dockerup/run.sh` from the docker directory.
or the Linux alias `<name_of_parent_dir>` from any directory.

Remember: you can specify a data directory at runtime with the `-d` flag: 
`.../run.sh -d ~/MyData/TestData` or by alias `<name_of_parent_dir> -d ~/MyData/TestData` <br />

otherwise it will default to your home dir.


#### Containerized Tools

* Ubuntu 20.04
* NVIDIA CUDA Toolkit
* ROS2 Foxy Fitzroy https://docs.ros.org/en/foxy/Installation.html


#### Other Flags
Other flags have been added for further functionality and refinement. 
* `ssudio -k` - to kill existing containers in case you get in a state where one is hanging
* `ssudio -r` also stops and zaps the stale container before rebuilding

