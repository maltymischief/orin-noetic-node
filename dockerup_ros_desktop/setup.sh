#!/usr/bin/env bash

SCRIPT_LOCATION=$(cd `dirname $0` && pwd)
PROJECT_NAME=$(builtin cd $SCRIPT_LOCATION; cd ..; basename -- "$PWD")


ALIAS_SETUP="${PROJECT_NAME^^}_SET"

WORKSPACE_PATH="$(find .. -type d -name \*ws -print -quit)"
WORKSPACE_NAME="$(basename -- $WORKSPACE_PATH)"
[[ ! -z "$WORKSPACE_NAME" ]] && echo "WORKSPACE FOUND: $WORKSPACE_NAME" || echo "No ROS workspace found."
#pause
read -t 5 -n 1 -s -r -p "Press any key to continue..."
echo -e "\n"

WORKSPACE_DIR="$(builtin cd $SCRIPT_LOCATION; cd ..; realpath ./$WORKSPACE_NAME)/"


if [ "$(id -u)" == "0" ]; then
   echo "STOP! Don't run setup as root - it will mess things up."
   echo "ABORTING SETUP"
   exit 1
fi

#Gather requirements for parent project.
echo "Copying project pip requirements from $(builtin cd $SCRIPT_LOCATION; cd ..; realpath .)/"
cp "$(builtin cd $SCRIPT_LOCATION; cd ..; realpath .)/requirements.txt" $SCRIPT_LOCATION/pip-requirements.tmp


#Run these commands to be able to run docker without `sudo`. This is IMPORTANT for setup.
#More info here: https://docs.docker.com/engine/install/linux-postinstall/
#sudo groupadd docker
#sudo usermod -aG docker $USER

#build the docker container
docker build \
   --build-arg USER=${PROJECT_NAME,,} \
   --build-arg UID=$(id -u ${USER}) \
   --build-arg GID=$(id -g ${USER}) \
   -t ${PROJECT_NAME,,}:devel \
   $SCRIPT_LOCATION

#if not already configured, set up aliases
if [[ -z  "${!ALIAS_SETUP}" ]]; then
   echo "Configure environment variables and aliases"
   #alias run command so we don't need to go to the dir to run it.
   echo -e "\n# ${PROJECT_NAME^^} CONFIGURATION" >> /home/$USER/.bashrc
   
   #set environment variable for location of this repo
   #echo export $WORKSPACE_DIR >> /home/$USER/.bashrc
   
   #create an alias command to launch the container
   echo -e alias ${PROJECT_NAME,,}="${SCRIPT_LOCATION}/run.sh" >> /home/$USER/.bashrc
   #set an environment flag to prevent aliasing again if setup is rerun.
   echo export ${PROJECT_NAME^^}_SET="true" >> /home/$USER/.bashrc
   exec bash
fi

#create container_bash_history"
touch $SCRIPT_LOCATION/container_bash_history.log
