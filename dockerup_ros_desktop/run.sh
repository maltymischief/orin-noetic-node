#docker run -it orin:v0 bash

#!/usr/bin/env bash

SCRIPT_LOCATION=$(cd `dirname $0` && pwd)
PROJECT_NAME=$(builtin cd $SCRIPT_LOCATION; cd ..; basename -- "$PWD")

WORKSPACE_PATH="$(find .. -type d | grep ws)"
WORKSPACE_NAME="$(basename -- $WORKSPACE_PATH)"
[[ ! -z "$WORKSPACE_NAME" ]] && echo "WORKSPACE FOUND: $WORKSPACE_NAME" || echo "No ROS workspace found."

WORKSPACE_DIR="$(builtin cd $SCRIPT_LOCATION; cd ..; realpath ./$WORKSPACE_NAME)/"


REBUILD='false'
KILL='false'


while getopts :d:p:rk flag
do
    case "${flag}" in
        'd') 
            HOST_DIR=${OPTARG};;
        'p') 
            PORTNUM=${OPTARG};;
        'r') 
            REBUILD='true';;
        'k') 
            KILL='true';;
    esac
done

if [ "$HOST_DIR" != "" ]; then
    echo "Data directory: $HOST_DIR";
    ### Check if directory does not exist ###
    if [ ! -d "$HOST_DIR" ] 
    then
        echo "Directory $HOST_DIR DOES NOT EXIST."
        exit 9999 # die with error code 9999
    fi
else
    HOST_DIR="$HOME"
fi

if [ "$PORTNUM" != "" ]; then
    echo "Port number: $PORTNUM";
else
    PORTNUM="8888"
    echo "Defaulting to port number: $PORTNUM";
fi

#docker_up()
#Brief: Check status of container and perform appropriate action.
#       Removes stale container and launches new or running container.
docker_up(){
    #No existing container, launch one.
    if [[ ! $(docker ps -a -q -f name=${PROJECT_NAME,,}) ]]; then 
        echo "Fire up a fresh container!"
        launch_container
    #Container alread exists
    else 
        id=$(docker ps -q -f name=${PROJECT_NAME,,} -f status=running)
        #Container is running, come on in.
        if [[ $id ]]; then  
            echo "Entering running ${PROJECT_NAME,,} container id $id ..."
            docker exec -it ${PROJECT_NAME,,} bash
        #Container is stopped - remove old container and launch a fresh one.
        else 
            kill_container
            launch_container
        fi
    fi
}

#check_is_super()
#Brief: Check if user is superuser, and exit script immediately if they are. 
check_is_super(){
    if [ "$(id -u)" == "0" ]; then
        echo "STOP! Don't run setup as root - it will mess things up."
        echo "ABORTING SETUP"
        exit 1 
    fi
}

#rebuild()
#Brief: Rebuilds the docker image. If container already exists, zap it and rebuild.
rebuild(){
    check_is_super   
    echo "Rebuilding ${PROJECT_NAME} Container"
    kill_container
    docker build \
    --build-arg USER=${PROJECT_NAME,,} \
    --build-arg UID=$(id -u ${USER}) \
    --build-arg GID=$(id -g ${USER}) \
    -t ${PROJECT_NAME,,}:devel \
    $SCRIPT_LOCATION

    touch "$SCRIPT_LOCATION/container_bash_history.log" #create bash log file
}

#kill()
#Brief: Kill and remove running project container.
kill_container(){
    id=$(docker ps -q -f name=${PROJECT_NAME,,})
    echo "Removing stale ${PROJECT_NAME,,} container $id..."
    #docker kill ssudio
    __=$(docker rm ${PROJECT_NAME,,} --force)
}


#launch_container()
#Brief: Launches docker container with all the requisite parameters. 
launch_container(){
    #run the docker container, map display, data dir, and code repo
    echo "Mapping host volume $HOST_DIR"
    echo "Mapping ROS workspace from ${WORKSPACE_DIR}"
    docker run -it --net=host \
        -p 0.0.0.0:6006:6006 \
        -p $PORTNUM:$PORTNUM \
        -v "${SCRIPT_LOCATION}/container_bash_history.log":/home/${PROJECT_NAME,,}/.bash_history \
        -v $HOST_DIR:/home/${PROJECT_NAME,,}/host_volume \
        -v "${WORKSPACE_DIR}":/home/${PROJECT_NAME,,}/$WORKSPACE_NAME \
        --name /${PROJECT_NAME,,} \
	    ${PROJECT_NAME,,}:devel bash
}

if "$REBUILD"; then
    check_is_super
    rebuild
elif "$KILL"; then
    kill_container
else
    docker_up
fi
